# PyCon2023 - GPT or how I learned to stop worrying and love the generative AI

## Getting started

Welcome to the Gitlab repository where I have shared the material for the IT PyCon2023 talk "GPT or how I learned to stop worrying and love the generative AI". Here you can find slides and a Colab with an introductory course of how to generate text from scratch by using [🤗 Transformers](https://huggingface.co/docs/transformers/index).

Enjoy yourself!

Please write an email to my address for any comments: [Tommaso Radicioni](mailto:tommaso.radicioni@aiknowyou.ai)